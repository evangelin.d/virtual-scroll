import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
 	{ path: '', redirectTo: 'user', pathMatch: 'full' },
	{ path: 'user', loadChildren: '../app/user/user.module#UserModule',  },
	

];


@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }