import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,FormControl,FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserService } from './../component/services/user.service';
import { Observable } from 'rxjs/Observable';
import { Http } from "@angular/http";
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css'],
  providers: [UserService]
})

export class UserFormComponent implements OnInit {
  public userForm: FormGroup;
  public user_list:Array<Object>;
  public id :any;
  private allItems: any[];
  pager: any = {};
  pagedItems: any[];

constructor(private fb: FormBuilder, private us: UserService,public route: ActivatedRoute,
  public router: Router) { 
    
} 

  ngOnInit() { 
     
  	this.userForm = this.fb.group({
      id:[''],
  		name: ['', [Validators.required,
        Validators.minLength(4),
        Validators.maxLength(24),
        Validators.pattern(/^[a-zA-Z0-9\-_.,!@#$%^*();:,\s]+$/)]],
  		username: ['', Validators.required],
  		 email: ['',
        [Validators.required,
          Validators.pattern(/^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        ]
        ],
      phone: ['', [Validators.required,
        Validators.minLength(10),
        Validators.maxLength(12),
        Validators.pattern(/^[0-9]+$/)]
      ],
      website: ['',
        [Validators.required,
          Validators.pattern(/^[a-zA-Z0-9\-_.,!#$%^*();:,\s]+$/),
        ]
        ],
 
    })

    if(this.id){       
    }
  }

submit(){
  
      let data = this.userForm.value;
      this.us.createUser(data).subscribe(result =>{
      console.log(result);
      this.us.getAll().subscribe(result =>{
      this.user_list =result;
      this.user_list.push(data);
      console.log(this.user_list);
      console.log("#########");
  })
  })
  
}

  getdata(){
     this.us.getAll().subscribe(result =>{
        console.log(result);
  })
  }

  particulardata(){
     this.us.getUser(this.id).subscribe(result =>{
       this.id = result.id;
       console.log(this.id);
  })
  }
}
