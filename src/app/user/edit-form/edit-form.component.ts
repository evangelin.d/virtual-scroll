import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,FormControl,FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserService } from './../component/services/user.service';
import { Observable } from 'rxjs/Observable';
import { Http } from "@angular/http";
import {Router, ActivatedRoute} from '@angular/router'; 

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css'],
  providers: [UserService]

})
export class EditFormComponent implements OnInit {
  public isEditForm: Boolean;
  public userForm: FormGroup;
  public user_id:any;

  constructor(private fb: FormBuilder, private us: UserService,public route: ActivatedRoute,
  public router: Router) { }

  ngOnInit() {
  	this.route.params.subscribe(params =>{
  		this.user_id = params['id']
  	})
  	this.userForm = this.fb.group({
      // userId:[''],
      id: ['', Validators.required],
      // title: ['', Validators.required],
      // completed: ['', Validators.required],
     
  		name: ['', Validators.required],
  		username: ['', Validators.required],
  		email: ['', Validators.required],
      phone: ['', Validators.required],
      website: ['', Validators.required]
 
    })
  
  this.Editdata();
}

Editdata(){
 
    this.us.getUser(this.user_id).subscribe(result =>{
      console.log(result);
      let userlist_datas = result;
      this.userForm['controls']['id'].setValue(this.user_id);
      this.userForm['controls']['name'].setValue(userlist_datas.name);
      this.userForm['controls']['username'].setValue(userlist_datas.username);
      this.userForm['controls']['email'].setValue(userlist_datas.email);
      this.userForm['controls']['phone'].setValue(userlist_datas.phone);
      this.userForm['controls']['website'].setValue(userlist_datas.website);
      
    })
  }

onUpdateSubmit(){
    
    let data = this.userForm.value;
    this.us.updateUser(data).subscribe(result =>{
      console.log(result);
      let path = 'user/list';
      this.router.navigate(['/user/list']);
      
    })
  }
}
