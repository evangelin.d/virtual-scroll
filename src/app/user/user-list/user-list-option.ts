export const listConfig = {
  rowData: [],
  columnDefs: [
    {headerName: 'Name', field: 'name', class: 'col-md'},
    {headerName: 'UserName', field: 'username', class: 'col-md'},
    {headerName: 'Email', field: 'email', class: 'col-md'},
    {headerName: 'Phone', field: 'phone', class: 'col-md'},
    {headerName: 'Website', field: 'website', class: 'col-md'},

  ],
  rowMenu: [],
  isSubGrouping: false,
};
