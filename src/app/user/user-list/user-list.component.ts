import { Component, OnInit, ViewChild, Directive, HostListener, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {CommonModule} from '@angular/common';
import {UserService} from './../component/services/user.service';
import {Sort} from '@angular/material';
import { listConfig } from './user-list-option';
import { DataSource } from '@angular/cdk/collections';
import { MatButtonModule, MatCheckboxModule,MatInputModule, MatTableModule,
MatFormFieldModule, MatRadioModule, MatSelectModule, MatSliderModule, MatMenuModule,
MatSidenavModule, MatToolbarModule, MatTabsModule, MatButtonToggleModule, MatIconModule,
MatProgressBarModule, MatTooltipModule, MatSnackBarModule, MatSlideToggleModule,PageEvent } from '@angular/material';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { Pipe, PipeTransform } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
// import { InfiniteScroll } from 'angular2-infinite-scroll';
import { ScrollEvent } from 'ngx-scroll-event';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import { ChangeEvent } from 'angular2-virtual-scroll';
import { VirtualScrollComponent } from 'angular2-virtual-scroll';

@Component({
  
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.css'],
	providers:[UserService]
})
export class UserListComponent implements OnInit {
	public user_list:any;
	public id:any;
	public columnDefs:any;
	public gridOptions;
	public sortedData;
  public length = 10;
  public key: string = 'name'; 
  public reverse: boolean = false;
   
// desserts = [
//     {id: '1', name: 'Leanne Graham', username: 'Bret', email: 'Sincere@april.biz  ', phone: '1-770-736-8031 x56442', website: 'hildegard.org'},
//     {id: '2', name: 'Ervin Howell', username: 'Antonette', email: 'Shanna@melissa.tv  ', phone: '010-692-6593 x09125', website: 'anastasia.net'},
//     {id: '3', name: 'Clementine Bauch', username: 'Samantha', email: 'Nathan@yesenia.net', phone: '1-463-123-4447', website: 'ramiro.info'},
//     {id: '4', name: 'Patricia Lebsack', username: 'Karianne ', email: 'Julianne.OConner@kory.org', phone: '493-170-9623 x156 ', website: 'kale.biz'},
//     {id: '5', name: ' Chelsey Dietrich ', username: 'Kamren', email: 'Lucio_Hettinger@annie.ca ', phone: '(254)954-1289', website: 'demarco.info '},
//     {id: '6', name: 'Mrs. Dennis Schulist ', username: 'Leopoldo_Corkery', email: 'Karley_Dach@jasper.info ', phone: '1-477-935-8478 x6430', website: 'ola.org'},
//     {id: '7', name: 'Kurtis Weissnat', username: 'Elwyn.Skiles', email: 'Telly.Hoeger@billy.biz', phone: '210.067.6132', website: 'elvis.io'},
//     {id: '8', name: 'Nicholas Runolfsdottir V ', username: 'Maxime_Nienow', email: 'Sherwood@rosamond.me', phone: '586.493.6943 x140', website: 'jacynthe.com'},
//     {id: '9', name: 'Glenna Reichert ', username: 'Delphine', email: 'Chaim_McDermott@dana.io', phone: '(775)976-6794 x41206', website: 'conrad.com'},                
//     {id: '10', name: 'Clementina DuBuque', username: 'Moriah.Stanton', email: 'Rey.Padberg@karina.biz', phone: ' 024-648-3804', website: 'ambrose.net'},
//   ];

constructor(private router: Router,public us:UserService,private route: ActivatedRoute, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer)
	
  {
		     // this.user_list = this.desserts.slice();
  


	}
   

	ngOnInit() {
    this.gridOptions = listConfig;
 		this.us.getAll().subscribe(
		data => {
			this.user_list = data;
			console.log(data);
      console.log(this.user_list);
			}, 
			err => {
			console.log(err);
			});

}


   

  // sortData(sort: Sort) {
  //   const data = this.desserts.slice();
  //   if (!sort.active || sort.direction == '') {
  //     this.user_list = data;
  //     return;
  //   }

  //   this.user_list = data.sort((a, b) => {
  //     let isAsc = sort.direction == 'asc';
  //     switch (sort.active) {
  //       case 'name': return compare(a.name, b.name, isAsc);
  //       case 'username': return compare(+a.username, +b.username, isAsc);
  //       case 'email': return compare(+a.email, +b.email, isAsc);
  //       case 'phone': return compare(+a.phone, +b.phone, isAsc);
  //       case 'website': return compare(+a.website, +b.website, isAsc);
  //       default: return 0;
  //     }
  //   });
  // }
  
deletedata(user){
   let selected = this.user_list.filter((x) => x.selected)
   console.log(selected);
    this.us.deleteUser(selected[0]['id']).subscribe(result =>{
      this.user_list = this.user_list.filter(d => {
                if (d.id != selected[0].id) {
                  return d;
                }
            })
      })
   // user.selected = (user.selected) ? false : true;
 }



checkbox(user) {
 	   user.selected = (user.selected) ? false : true;
    }

onScroll(event) {
    console.log('scroll event', event);
  }

@ViewChild(VirtualScrollComponent)
  private virtualScroll: VirtualScrollComponent;
  afterResize() {
  this.virtualScroll.refresh();

}

}


function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
    