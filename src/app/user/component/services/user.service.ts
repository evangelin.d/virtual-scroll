import { Injectable } from '@angular/core';
import { Http, Response, Headers, Jsonp, RequestOptions } from '@angular/http';
import { AppSettings } from './../../../app-setting';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';


@Injectable()
export class UserService {
	
	public user_list:Array<Object>;
	public user_details:any;
	public actionUrl:string;
	public headers:Headers;
	public options:RequestOptions;
	public user_item:any;
	
	constructor(private _http: Http, private _jsonp: Jsonp) 
	{ 
	    this.actionUrl ='https://jsonplaceholder.typicode.com/todos';
		this.headers = new Headers();
		this.user_list = [];
		this.user_item = [];
	}

	setURL()
	{
		this.actionUrl = 'https://jsonplaceholder.typicode.com/todos/';

	}
  
	getAll(){

  		this.setURL();
   		return this._http.get(this.actionUrl)
        .map((res:Response)=> {
        	this.user_list  = res.json();
        	return  this.user_list ;
        })
        .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  	}	
  	getUser(id:any){
  		return this._http.get(this.actionUrl+id+"/")
  		.map((res:Response)=> {
  			this.user_item = res.json();
  			return this.user_item;
  		})

  	} 

	createUser(user_data:any){	
		console.log(user_data)
		// let url =  AppSettings.API_ENDPOINT+'user/';
		return Observable.forkJoin(
			this._http.post(this.actionUrl, user_data)
			.map((res: Response) => res.json())
			);     

	}

	deleteUser(id:any){
		
		return this._http.delete(this.actionUrl+id+"/")
		.map((res:Response)=> res.json()) 
		.catch((error:any)=> Observable.throw(error.json().error || 'Server error'));

		}
	updateUser(data){
		console.log(data);
		return Observable.forkJoin(
		this._http.put(this.actionUrl+data.id+"/", data)
		.map((res:Response)=> res.json())

	);
	}
	}
	