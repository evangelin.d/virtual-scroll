import { UserComponent } from './user/user.component';
import { UserFormComponent } from '../user-form/user-form.component';
import { UserListComponent } from '../user-list/user-list.component';
import { EditFormComponent } from '../edit-form/edit-form.component';
import { ListItemComponent } from '../list-item/list-item.component';
export const USER_COMPONENTS = [
  UserComponent,
  UserFormComponent,
  UserListComponent,
  EditFormComponent,
  ListItemComponent

];

export const USER_ROUTES = [
{
  path: '', component: UserComponent,
  children: [
      { path: '', redirectTo: 'list', pathMatch: 'full' },
      { path: 'list', component: UserListComponent },
      { path: 'form', component: UserFormComponent },
      { path: 'edit/:id', component: EditFormComponent },
      { path: 'listitem', component: ListItemComponent },
      ]
}];