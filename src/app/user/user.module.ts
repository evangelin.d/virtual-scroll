import {NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {UserRoutingModule} from './user-routing.module';
import {USER_COMPONENTS} from './component';
import {CommonModule} from '@angular/common';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { MatButtonModule, MatCheckboxModule,MatInputModule, MatTableModule,
MatFormFieldModule, MatRadioModule, MatSelectModule, MatSliderModule, MatMenuModule,
MatSidenavModule, MatToolbarModule, MatTabsModule, MatButtonToggleModule, MatIconModule,
MatProgressBarModule, MatTooltipModule, MatSnackBarModule,MatGridListModule,
MatPaginatorModule, MatSortModule, MatProgressSpinnerModule } from '@angular/material';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import { Pipe, PipeTransform } from '@angular/core';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
// import { NgInfiniteScrollModule } from 'ngx-sentinel-infinite-scroll';
import { ScrollEventModule } from 'ngx-scroll-event';
import { VirtualScrollModule } from 'angular2-virtual-scroll';
import { ListItemComponent } from './list-item/list-item.component';



@NgModule({
	imports: [
	FormsModule,
	ReactiveFormsModule,
	CommonModule,
	MatInputModule,
	MatButtonModule,
	MatCheckboxModule,
	MatTableModule,
	MatFormFieldModule, 
	MatRadioModule, 
	MatSelectModule, 
	MatSliderModule, 
	MatMenuModule,
    MatSidenavModule, 
    MatToolbarModule, 
    MatTabsModule, 
    MatButtonToggleModule, 
    MatIconModule,
    MatProgressBarModule, 
    MatTooltipModule, 
    MatSnackBarModule,
    UserRoutingModule,
    MatGridListModule,
    MatPaginatorModule,
    MatSortModule,
    Ng2OrderModule,
    NgxPaginationModule,
    InfiniteScrollModule,
    ScrollEventModule,
    VirtualScrollModule,
    MatProgressSpinnerModule

 
    // Ng2TableModule

	  ],
	declarations: [...USER_COMPONENTS, ListItemComponent],
	})
export class UserModule {
	
}