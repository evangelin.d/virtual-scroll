import {NgModule} from '@angular/core';
import {USER_ROUTES} from './component';
import {Routes, RouterModule} from '@angular/router';
import {Component, ViewChild} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';



const userRoutes: Routes = [...USER_ROUTES];

@NgModule({
	imports: [RouterModule.forChild(userRoutes)	],
	exports: [RouterModule]

})
export class UserRoutingModule {

}
