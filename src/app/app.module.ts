import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule, JsonpModule, RequestOptions } from '@angular/http';
import { DataSource } from '@angular/cdk/collections';
import { MatButtonModule, MatCheckboxModule,MatInputModule, MatTableModule,
MatFormFieldModule, MatRadioModule, MatSelectModule, MatSliderModule, MatMenuModule,
MatSidenavModule, MatToolbarModule, MatTabsModule, MatButtonToggleModule, MatIconModule,
MatProgressBarModule, MatTooltipModule, MatSnackBarModule, MatSlideToggleModule,MatGridListModule,
MatPaginatorModule, MatSortModule, MatProgressSpinnerModule } from '@angular/material';
import { NoConflictStyleCompatibilityMode } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Ng2OrderModule } from 'ng2-order-pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import { Pipe, PipeTransform } from '@angular/core';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
// import { NgInfiniteScrollModule } from 'ngx-sentinel-infinite-scroll';
import { ScrollEventModule } from 'ngx-scroll-event';
import { VirtualScrollModule } from 'angular2-virtual-scroll';


@NgModule({
  declarations: [
    AppComponent,
    
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    CommonModule,
    HttpModule,
    JsonpModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTableModule,
    BrowserAnimationsModule,
    NoConflictStyleCompatibilityMode,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule, 
    MatRadioModule, 
    MatSelectModule, 
    MatSliderModule, 
    MatMenuModule,
    MatSidenavModule, 
    MatToolbarModule, 
    MatTabsModule, 
    MatButtonToggleModule, 
    MatIconModule,
    MatProgressBarModule, 
    MatTooltipModule, 
    MatSnackBarModule,
    MatSlideToggleModule,
    MatGridListModule,
    MatPaginatorModule,
    MatSortModule,
    Ng2OrderModule,
    NgxPaginationModule,
    InfiniteScrollModule,
    ScrollEventModule,
    VirtualScrollModule,
    MatProgressSpinnerModule
  ],
  exports: [
   
    FormsModule,
    ReactiveFormsModule,
    CommonModule
    // MaterialModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

